package by.home.work.myProject.repository;

import by.home.work.myProject.pojo.Contract;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContractRepository extends CrudRepository<Contract, Integer> {

    List<Contract> findByName (String name);

}
